<?php

return [

    /*
     * Default disk for generated image
     */
    'disk' => 'local',

    /*
     * Default path for generated image
     */
    'path' => 'dummyimage',

    /*
     * Additional headers for response
     */
    'headers' => [
        //
    ],

    /*
     * Additional html color names with hex value for name to hex conversion
     */
    'color_names' => [
        //'color_name' => '00ffff'
    ],
];
